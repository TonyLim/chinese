This project have two main interface :

    An API REST interface with Swagger
    A web interface with Angular

The purpose of this project is to store chinese vocabulary and design a web interface to learn those words.
It is also a good training exercice for me to build from scratch a small web application using an API I also built.
Those words and lessons come from the book : 301 Phrases dans Les Conversations en Chinois - Tome 1 meaning 301 sentences for conversational chinese.

More information in the wiki
https://gitlab.com/TonyLim/chinese/wikis/home