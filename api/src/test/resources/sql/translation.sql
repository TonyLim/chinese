INSERT INTO lesson (lesson_id, number) select 1,1;
INSERT INTO lesson (lesson_id, number) select 2,2;

INSERT INTO word (word_id, pinyin, logogram, lesson_id) select nextval('word_seq'),'wǒ','我',1;
INSERT INTO translation (translation_id, word_id, meaning) select nextval('translation_seq'),currval('word_seq'),'Moi';
INSERT INTO translation (translation_id, word_id, meaning) select nextval('translation_seq'),currval('word_seq'),'Je';

INSERT INTO word (word_id, pinyin, logogram, lesson_id) select nextval('word_seq'),'nǐ','伱',2;
INSERT INTO translation (translation_id, word_id, meaning) select nextval('translation_seq'),currval('word_seq'),'Toi';
INSERT INTO translation (translation_id, word_id, meaning) select nextval('translation_seq'),currval('word_seq'),'Tu';