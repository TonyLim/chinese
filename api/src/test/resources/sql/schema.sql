drop table if exists user;
drop table if exists word;
drop table if exists translation;
drop table if exists lesson;

drop sequence if exists user_seq;
drop sequence if exists word_seq;
drop sequence if exists translation_seq;
drop sequence if exists lesson_seq;

create sequence user_seq start with 1 increment by 1;
create sequence word_seq start with 1 increment by 1;
create sequence translation_seq start with 1 increment by 1;
create sequence lesson_seq start with 1 increment by 1;

create table user (user_id bigint not null, first_name varchar(255), last_name varchar(255), primary key (user_id));
create table word (word_id bigint not null, pinyin varchar(255), logogram varchar(255), lesson_id bigint, primary key (word_id));
create table translation (translation_id bigint not null, word_id bigint not null, meaning varchar(255), primary key (translation_id));
create table lesson (lesson_id bigint not null, number bigint not null, primary key (lesson_id));