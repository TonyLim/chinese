package com;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootApiTestLauncher {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootApiTestLauncher.class, args);
	}
}
