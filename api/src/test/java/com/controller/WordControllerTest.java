package com.controller;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Before;
import org.junit.Test;
import org.springframework.http.MediaType;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.SqlConfig;

import com.AbstractControllerTest;

@Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = {"classpath:sql/schema.sql", 
		"classpath:sql/translation.sql"}, config = @SqlConfig(encoding = "utf-8"))
public class WordControllerTest extends AbstractControllerTest {

	@Override
	@Before
	public void setUp() {
		super.setUp();
	}

	@Test
	public void shouldFindAll() throws Exception {
		mvc.perform(get("/words").contentType(MediaType.APPLICATION_JSON))
		.andExpect(status().isOk())
		.andExpect(jsonPath("$", hasSize(2)))
		.andExpect(jsonPath("$[0].id", is(1)))
		.andExpect(jsonPath("$[0].pinyin", is("wǒ")))
		.andExpect(jsonPath("$[0].logogram", is("我")))
		.andExpect(jsonPath("$[0].lessonId", is(1)))
		.andExpect(jsonPath("$[0].translations[0].meaning", is("Moi")))
		.andExpect(jsonPath("$[0].translations[1].meaning", is("Je")))
		.andExpect(jsonPath("$[1].id", is(2)))
		.andExpect(jsonPath("$[1].pinyin", is("nǐ")))
		.andExpect(jsonPath("$[1].logogram", is("伱")))
		.andExpect(jsonPath("$[1].lessonId", is(2)))
		.andExpect(jsonPath("$[1].translations[0].meaning", is("Toi")))
		.andExpect(jsonPath("$[1].translations[1].meaning", is("Tu")));
	}
	
	@Test
	public void shouldFindByLesson() throws Exception {
		mvc.perform(get("/words?lessonId=2").contentType(MediaType.APPLICATION_JSON))
		.andExpect(status().isOk())
		.andExpect(jsonPath("$", hasSize(1)))
		.andExpect(jsonPath("$[0].id", is(2)))
		.andExpect(jsonPath("$[0].pinyin", is("nǐ")))
		.andExpect(jsonPath("$[0].logogram", is("伱")))
		.andExpect(jsonPath("$[0].lessonId", is(2)))
		.andExpect(jsonPath("$[0].translations[0].meaning", is("Toi")))
		.andExpect(jsonPath("$[0].translations[1].meaning", is("Tu")));
	}

}
