package com.controller;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Before;
import org.junit.Test;
import org.springframework.http.MediaType;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.SqlConfig;

import com.AbstractControllerTest;

@Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = {"classpath:sql/schema.sql", 
		"classpath:sql/user.sql"}, config = @SqlConfig(encoding = "utf-8"))
public class UserControllerTest extends AbstractControllerTest {

	@Override
	@Before
	public void setUp() {
		super.setUp();
	}

	@Test
	public void shouldFindAll() throws Exception {
		mvc.perform(get("/users").contentType(MediaType.APPLICATION_JSON))
		.andExpect(status().isOk())
		.andExpect(jsonPath("$", hasSize(2)))
		.andExpect(jsonPath("$[0].id", is(1)))
		.andExpect(jsonPath("$[0].firstName", is("Tony")))
		.andExpect(jsonPath("$[0].lastName", is("Lim")))
		.andExpect(jsonPath("$[1].id", is(2)))
		.andExpect(jsonPath("$[1].firstName", is("Salomé")))
		.andExpect(jsonPath("$[1].lastName", is("Rivoallanou-Drevet")));
	}

}
