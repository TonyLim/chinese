package com.controller;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Before;
import org.junit.Test;
import org.springframework.http.MediaType;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.SqlConfig;

import com.AbstractControllerTest;

@Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = {"classpath:sql/schema.sql", 
		"classpath:sql/translation.sql"}, config = @SqlConfig(encoding = "utf-8"))
public class TranslationControllerTest extends AbstractControllerTest {

	@Override
	@Before
	public void setUp() {
		super.setUp();
	}

	@Test
	public void shouldFindAll() throws Exception {
		mvc.perform(get("/translations").contentType(MediaType.APPLICATION_JSON))
		.andExpect(status().isOk())
		.andExpect(jsonPath("$", hasSize(4)))
		.andExpect(jsonPath("$[0].id", is(1)))
		.andExpect(jsonPath("$[0].wordId", is(1)))
		.andExpect(jsonPath("$[0].meaning", is("Moi")))
		.andExpect(jsonPath("$[1].id", is(2)))
		.andExpect(jsonPath("$[1].wordId", is(1)))
		.andExpect(jsonPath("$[1].meaning", is("Je")))
		.andExpect(jsonPath("$[2].id", is(3)))
		.andExpect(jsonPath("$[2].wordId", is(2)))
		.andExpect(jsonPath("$[2].meaning", is("Toi")))
		.andExpect(jsonPath("$[3].id", is(4)))
		.andExpect(jsonPath("$[3].wordId", is(2)))
		.andExpect(jsonPath("$[3].meaning", is("Tu")));
	}

}
