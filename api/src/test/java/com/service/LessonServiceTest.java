package com.service;

import static org.assertj.core.api.Assertions.assertThat;
import java.util.List;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.SqlConfig;
import org.springframework.transaction.annotation.Transactional;

import com.AbstractTest;
import com.dto.LessonDto;
import com.dto.WordDto;

@Transactional
@Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = {"classpath:sql/schema.sql", 
		"classpath:sql/translation.sql"}, config = @SqlConfig(encoding = "utf-8"))
public class LessonServiceTest extends AbstractTest {
	
	@Autowired
	private LessonService lessonService;

	@Test
	public void shouldFindAll() {
		List<LessonDto> lessons = lessonService.findAll();
		assertThat(lessons).hasSize(2);
		final LessonDto lesson1 = lessons.get(0);
		final LessonDto lesson2 = lessons.get(1);
		assertThat(lesson1.getId()).isEqualTo(1L);
		assertThat(lesson1.getLessonNumber()).isEqualTo(1L);
		assertThat(lesson2.getId()).isEqualTo(2L);
		assertThat(lesson2.getLessonNumber()).isEqualTo(2L);
		final WordDto w1 = lesson1.getWords().get(0);
		final WordDto w2 = lesson2.getWords().get(0);
		assertThat(w1.getLessonId()).isEqualTo(1L);
		assertThat(w1.getPinyin()).isEqualTo("wǒ");
		assertThat(w1.getLogogram()).isEqualTo("我");
		assertThat(w1.getTranslations().get(0).getMeaning()).isEqualTo("Moi");
		assertThat(w1.getTranslations().get(1).getMeaning()).isEqualTo("Je");
		
		assertThat(w2.getLessonId()).isEqualTo(2L);
		assertThat(w2.getPinyin()).isEqualTo("nǐ");
		assertThat(w2.getLogogram()).isEqualTo("伱");
		assertThat(w2.getTranslations().get(0).getMeaning()).isEqualTo("Toi");
		assertThat(w2.getTranslations().get(1).getMeaning()).isEqualTo("Tu");
	}
}
