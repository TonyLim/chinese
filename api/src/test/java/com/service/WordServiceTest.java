package com.service;

import static org.assertj.core.api.Assertions.assertThat;
import java.util.List;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.SqlConfig;
import org.springframework.transaction.annotation.Transactional;

import com.AbstractTest;
import com.dto.WordDto;

@Transactional
@Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = {"classpath:sql/schema.sql", 
		"classpath:sql/translation.sql"}, config = @SqlConfig(encoding = "utf-8"))
public class WordServiceTest extends AbstractTest {
	
	@Autowired
	private WordService wordService;

	@Test
	public void shouldFindAll() {
		List<WordDto> words = wordService.findAll();
		assertThat(words).hasSize(2);
		final WordDto word1 = words.get(0);
		final WordDto word2 = words.get(1);
		assertThat(word1.getId()).isEqualTo(1L);
		assertThat(word1.getPinyin()).isEqualTo("wǒ");
		assertThat(word1.getLogogram()).isEqualTo("我");
		assertThat(word1.getLessonId()).isEqualTo(1L);
		assertThat(word1.getTranslations().get(0).getMeaning()).isEqualTo("Moi");
		assertThat(word1.getTranslations().get(1).getMeaning()).isEqualTo("Je");
		
		assertThat(word2.getId()).isEqualTo(2L);
		assertThat(word2.getPinyin()).isEqualTo("nǐ");
		assertThat(word2.getLogogram()).isEqualTo("伱");
		assertThat(word2.getLessonId()).isEqualTo(2L);
		assertThat(word2.getTranslations().get(0).getMeaning()).isEqualTo("Toi");
		assertThat(word2.getTranslations().get(1).getMeaning()).isEqualTo("Tu");
	}

	/*
	public void shouldCreate() {
		List<WordDto> wordsBefore = wordService.findAll();
		assertThat(wordsBefore).hasSize(2);
		
		WordDto newWord = new WordDto();
		newWord.setLessonId(2L);
		newWord.setPinyin("tā");
		newWord.setLogogram("他");
		wordService.save(newWord);

		List<WordDto> wordsAfter = wordService.findAll();
		assertThat(wordsAfter).hasSize(3);
		final WordDto word3 = wordsAfter.get(2);
		assertThat(word3.getId()).isEqualTo(3L);
		assertThat(word3.getLessonId()).isEqualTo(2L);
		assertThat(word3.getPinyin()).isEqualTo("tā");
		assertThat(word3.getLogogram()).isEqualTo("他");
	}

	public void shouldFindById() {
		WordDto word = wordService.findById(1L);
		assertThat(word.getId()).isEqualTo(1L);
		assertThat(word.getPinyin()).isEqualTo("wǒ");
		assertThat(word.getLogogram()).isEqualTo("我");
		assertThat(word.getTranslations().get(0).getMeaning()).isEqualTo("Moi");
		assertThat(word.getTranslations().get(1).getMeaning()).isEqualTo("Je");
	}
	*/

	@Test
	public void shouldFindByWord() {
		List<WordDto> words = wordService.findByLesson(2L);
		assertThat(words).hasSize(1);
		final WordDto word1 = words.get(0);
		
		assertThat(word1.getId()).isEqualTo(2L);
		assertThat(word1.getPinyin()).isEqualTo("nǐ");
		assertThat(word1.getLogogram()).isEqualTo("伱");
		assertThat(word1.getLessonId()).isEqualTo(2L);
		assertThat(word1.getTranslations().get(0).getMeaning()).isEqualTo("Toi");
		assertThat(word1.getTranslations().get(1).getMeaning()).isEqualTo("Tu");
	}
}
