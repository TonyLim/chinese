package com.service;

import static org.assertj.core.api.Assertions.assertThat;
import java.util.List;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.SqlConfig;

import com.AbstractTest;
import com.dto.UserDto;

@Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = {"classpath:sql/schema.sql", 
		"classpath:sql/user.sql"}, config = @SqlConfig(encoding = "utf-8"))
public class UserServiceTest extends AbstractTest {
	
	@Autowired
	private UserService userService;

	@Test
	public void shouldFindAll() {
		List<UserDto> users = userService.findAll();
		assertThat(users).hasSize(2);
		final UserDto user1 = users.get(0);
		final UserDto user2 = users.get(1);
		assertThat(user1.getId()).isEqualTo(1L);
		assertThat(user1.getFirstName()).isEqualTo("Tony");
		assertThat(user1.getLastName()).isEqualTo("Lim");
		
		assertThat(user2.getId()).isEqualTo(2L);
		assertThat(user2.getFirstName()).isEqualTo("Salomé");
		assertThat(user2.getLastName()).isEqualTo("Rivoallanou-Drevet");
	}

	@Test
	public void shouldCreate() {
		List<UserDto> usersBefore = userService.findAll();
		assertThat(usersBefore).hasSize(2);
		
		UserDto newUser = new UserDto();
		newUser.setFirstName("Warren");
		newUser.setLastName("Webber");
		userService.save(newUser);

		List<UserDto> usersAfter = userService.findAll();
		assertThat(usersAfter).hasSize(3);
		final UserDto user3 = usersAfter.get(2);
		assertThat(user3.getId()).isEqualTo(3L);
		assertThat(user3.getFirstName()).isEqualTo("Warren");
		assertThat(user3.getLastName()).isEqualTo("Webber");
	}

}
