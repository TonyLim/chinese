package com.service;

import static org.assertj.core.api.Assertions.assertThat;
import java.util.List;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.SqlConfig;
import org.springframework.transaction.annotation.Transactional;

import com.AbstractTest;
import com.dto.TranslationDto;

@Transactional
@Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = {"classpath:sql/schema.sql", 
		"classpath:sql/translation.sql"}, config = @SqlConfig(encoding = "utf-8"))
public class TranslationServiceTest extends AbstractTest {
	
	@Autowired
	private TranslationService translationService;

	@Test
	public void shouldFindAll() {
		List<TranslationDto> translations = translationService.findAll();
		assertThat(translations).hasSize(4);
		final TranslationDto translation1 = translations.get(0);
		final TranslationDto translation2 = translations.get(2);
		assertThat(translation1.getId()).isEqualTo(1L);
		assertThat(translation1.getWordId()).isEqualTo(1);
		assertThat(translation1.getMeaning()).isEqualTo("Moi");
		
		assertThat(translation2.getId()).isEqualTo(3L);
		assertThat(translation2.getWordId()).isEqualTo(2);
		assertThat(translation2.getMeaning()).isEqualTo("Toi");
	}

	public void shouldCreate() {
		List<TranslationDto> translationsBefore = translationService.findAll();
		assertThat(translationsBefore).hasSize(4);
		
		TranslationDto newTranslation = new TranslationDto();
		newTranslation.setWordId(2L);
		newTranslation.setMeaning("Vous");
		translationService.save(newTranslation);

		List<TranslationDto> translationsAfter = translationService.findAll();
		assertThat(translationsAfter).hasSize(5);
		final TranslationDto translation5 = translationsAfter.get(translationsAfter.size()-1);
		assertThat(translation5.getId()).isEqualTo(5L);
		assertThat(translation5.getWordId()).isEqualTo(2L);
		assertThat(translation5.getMeaning()).isEqualTo("Vous");
	}

}
