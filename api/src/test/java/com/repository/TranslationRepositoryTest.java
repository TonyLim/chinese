package com.repository;

import static org.assertj.core.api.Assertions.assertThat;
import java.util.List;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.SqlConfig;
import org.springframework.transaction.annotation.Transactional;

import com.AbstractTest;
import com.repository.TranslationRepository;

import com.entity.Translation;
import com.entity.Word;

@Transactional
@Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = {"classpath:sql/schema.sql", 
		"classpath:sql/translation.sql"}, config = @SqlConfig(encoding = "utf-8"))
public class TranslationRepositoryTest extends AbstractTest {
	
	@Autowired
	private TranslationRepository translationRepository;

	@Test
	public void shouldFindAll() {
		List<Translation> translations = translationRepository.findAll();
		assertThat(translations).hasSize(4);
		final Translation translation1 = translations.get(0);
		final Translation translation2 = translations.get(2);
		assertThat(translation1.getId()).isEqualTo(1L);
		assertThat(translation1.getMeaning()).isEqualTo("Moi");
		assertThat(translation1.getWord().getId()).isEqualTo(1);
		
		assertThat(translation2.getId()).isEqualTo(3L);
		assertThat(translation2.getMeaning()).isEqualTo("Toi");
		assertThat(translation2.getWord().getId()).isEqualTo(2);
	}

	/*
	public void shouldCreate() {
		List<Translation> translationsBefore = translationRepository.findAll();
		assertThat(translationsBefore).hasSize(4);
		
		Translation newTranslation = new Translation();
		Word w = new Word();
		w.setId(2L);
		w.setPinyin("nǐ");
		w.setLogogram("伱");
		newTranslation.setMeaning("Vous");
		newTranslation.setWord(w);
		translationRepository.save(newTranslation);

		List<Translation> translationsAfter = translationRepository.findAll();
		assertThat(translationsAfter).hasSize(5);
		final Translation translation5 = translationsAfter.get(translationsAfter.size()-1);
		assertThat(translation5.getId()).isEqualTo(5L);
		assertThat(translation5.getWord()).isEqualTo(2L);
		assertThat(translation5.getMeaning()).isEqualTo("Vous");
	}
	*/

	/*
	public void shouldFinByWordId() {
		List<Translation> translations = translationRepository.findByWordId(1L);
		assertThat(translations).hasSize(2);
		final Translation translation1 = translations.get(0);
		final Translation translation2 = translations.get(1);
		assertThat(translation1.getId()).isEqualTo(1L);
		assertThat(translation1.getWord().getId()).isEqualTo(1);
		assertThat(translation1.getMeaning()).isEqualTo("Moi");
		
		assertThat(translation2.getId()).isEqualTo(2L);
		assertThat(translation2.getWord().getId()).isEqualTo(1);
		assertThat(translation2.getMeaning()).isEqualTo("Je");
	}
	*/
	
}
