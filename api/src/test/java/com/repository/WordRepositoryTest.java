package com.repository;

import static org.assertj.core.api.Assertions.assertThat;
import java.util.List;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.SqlConfig;
import org.springframework.transaction.annotation.Transactional;

import com.AbstractTest;
import com.repository.WordRepository;
import com.entity.Word;

@Transactional
@Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = {"classpath:sql/schema.sql", 
		"classpath:sql/translation.sql"}, config = @SqlConfig(encoding = "utf-8"))
public class WordRepositoryTest extends AbstractTest {
	
	@Autowired
	private WordRepository wordRepository;

	@Test
	public void shouldFindAll() {
		List<Word> words = wordRepository.findAll();
		assertThat(words).hasSize(2);
		final Word word1 = words.get(0);
		final Word word2 = words.get(1);
		assertThat(word1.getId()).isEqualTo(1L);
		assertThat(word1.getLesson().getId()).isEqualTo(1L);
		assertThat(word1.getPinyin()).isEqualTo("wǒ");
		assertThat(word1.getLogogram()).isEqualTo("我");
		assertThat(word1.getTranslations().get(0).getMeaning()).isEqualTo("Moi");
		assertThat(word1.getTranslations().get(1).getMeaning()).isEqualTo("Je");
		
		assertThat(word2.getId()).isEqualTo(2L);
		assertThat(word2.getLesson().getId()).isEqualTo(2L);
		assertThat(word2.getPinyin()).isEqualTo("nǐ");
		assertThat(word2.getLogogram()).isEqualTo("伱");
		assertThat(word2.getTranslations().get(0).getMeaning()).isEqualTo("Toi");
		assertThat(word2.getTranslations().get(1).getMeaning()).isEqualTo("Tu");
	}

	/*
	public void shouldCreate() {
		List<Word> wordsBefore = wordRepository.findAll();
		assertThat(wordsBefore).hasSize(2);
		
		Lesson l = new Lesson();
		l.setId(2L);
		Word newWord = new Word();
		newWord.setLesson(l);
		newWord.setPinyin("tā");
		newWord.setLogogram("他");
		wordRepository.save(newWord);

		List<Word> wordsAfter = wordRepository.findAll();
		assertThat(wordsAfter).hasSize(3);
		final Word word3 = wordsAfter.get(2);
		assertThat(word3.getId()).isEqualTo(3L);
		assertThat(word3.getLesson().getId()).isEqualTo(2L);
		assertThat(word3.getPinyin()).isEqualTo("tā");
		assertThat(word3.getLogogram()).isEqualTo("他");
	}
	*/

	@Test
	public void shouldFindByLesson() {
		List<Word> words = wordRepository.findByLesson(2L).get();
		assertThat(words).hasSize(1);
		final Word word1 = words.get(0);
		assertThat(word1.getId()).isEqualTo(2L);
		assertThat(word1.getLesson().getId()).isEqualTo(2L);
		assertThat(word1.getPinyin()).isEqualTo("nǐ");
		assertThat(word1.getLogogram()).isEqualTo("伱");
		assertThat(word1.getTranslations().get(0).getMeaning()).isEqualTo("Toi");
		assertThat(word1.getTranslations().get(1).getMeaning()).isEqualTo("Tu");
	}
}
