package com;

import java.io.IOException;
import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public abstract class AbstractControllerTest extends AbstractTest {

	protected MockMvc mvc;

	@Autowired
	protected WebApplicationContext webApplicationContext;

	protected void setUp() {
		mvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
	}

	// Jackson mapper
	private final ObjectMapper mapper = new ObjectMapper();

	protected String toJson(Object o) throws JsonProcessingException {
		return mapper.writeValueAsString(o);
	}

	protected String toJson(Collection<?> os) throws JsonProcessingException {
		return mapper.writeValueAsString(os);
	}

	protected Object toObject(String json, Class clazz) throws JsonParseException, JsonMappingException, IOException {
		return mapper.readValue(json, clazz);
	}

	protected Object toObject(ResultActions r, Class clazz)
			throws JsonParseException, JsonMappingException, IOException {
		return toObject(r.andReturn().getResponse().getContentAsString(), clazz);
	}

}
