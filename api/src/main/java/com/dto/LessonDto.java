package com.dto;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class LessonDto {
	private Long id;
	private Long lessonNumber;
	private List<WordDto> words;
}
