package com.dto;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class WordDto {
	private Long id;
	private String pinyin;
	private String logogram;
	private Long lessonId;
	private List<TranslationDto> translations;
}
