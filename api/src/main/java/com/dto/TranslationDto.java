package com.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TranslationDto {
	private Long id;
	private Long wordId;
	private String meaning;
}
