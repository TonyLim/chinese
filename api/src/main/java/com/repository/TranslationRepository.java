package com.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.entity.Translation;

@Repository
public interface TranslationRepository extends JpaRepository<Translation, Long>{
	/*
	@Query("SELECT t FROM translation t WHERE word_id = :wordId")
	public List<Translation> findByWord(@Param("wordId") Long wordId);
	*/
}
