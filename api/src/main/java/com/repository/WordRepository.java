package com.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.entity.Word;

@Repository
public interface WordRepository extends JpaRepository<Word, Long>{

	@Query("SELECT w FROM Word w WHERE lesson_id = :lessonId")
	public Optional<List<Word>> findByLesson(@Param("lessonId") Long lessonId);
}
