package com.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.entity.Lesson;
import com.config.AbstractController;
import com.dto.LessonDto;
import com.service.LessonService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.ApiResponse;

@RestController
@RequestMapping("/lessons")
@Api(tags = { "Lesson" }, value = "/lessons", produces = "application/json", consumes = "application/json")
public class LessonController implements AbstractController {

	private final LessonService lessonService;

	public LessonController(@Autowired LessonService lessonService) {
		this.lessonService = lessonService;
	}
	
	@GetMapping
	@ResponseStatus(value = HttpStatus.OK)
	@ApiOperation(httpMethod = "GET", produces = "application/json", response = Lesson.class, value = "Find existing lesson")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "OK"), @ApiResponse(code = 204, message = "No Content"),
			@ApiResponse(code = 400, message = "Bad Request"), @ApiResponse(code = 401, message = "Unauthorized"),
			@ApiResponse(code = 403, message = "Forbidden"), @ApiResponse(code = 404, message = "Not Found") })
	public ResponseEntity<List<LessonDto>> find() {
		return ResponseEntity.ok(lessonService.findAll());
	}
}














