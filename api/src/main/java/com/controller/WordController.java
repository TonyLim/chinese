package com.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.entity.Word;
import com.config.AbstractController;
import com.dto.WordDto;
import com.service.WordService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.ApiResponse;

@RestController
@RequestMapping("/words")
@Api(tags = { "Word" }, value = "/words", produces = "application/json", consumes = "application/json")
public class WordController implements AbstractController {

	private final WordService wordService;

	public WordController(WordService wordService) {
		this.wordService = wordService;
	}
	
	@GetMapping
	@ResponseStatus(value = HttpStatus.OK)
	@ApiOperation(httpMethod = "GET", produces = "application/json", response = Word.class, value = "Find existing word")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "OK"), @ApiResponse(code = 204, message = "No Content"),
			@ApiResponse(code = 400, message = "Bad Request"), @ApiResponse(code = 401, message = "Unauthorized"),
			@ApiResponse(code = 403, message = "Forbidden"), @ApiResponse(code = 404, message = "Not Found") })
	public ResponseEntity<List<WordDto>> find(@RequestParam(name = "lessonId", required = false) Long lessonId) {
		if(lessonId != null) {
			return ResponseEntity.ok(wordService.findByLesson(lessonId));
		} else {
			return ResponseEntity.ok(wordService.findAll());
		}
	}
}














