package com.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.entity.User;
import com.config.AbstractController;
import com.dto.UserDto;
import com.service.UserService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.ApiResponse;

@RestController
@RequestMapping("/users")
@Api(tags = { "User" }, value = "/users", produces = "application/json", consumes = "application/json")
public class UserController implements AbstractController {

	private final UserService userService;

	public UserController(@Autowired UserService userService) {
		this.userService = userService;
	}
	
	@GetMapping
	@ResponseStatus(value = HttpStatus.OK)
	@ApiOperation(httpMethod = "GET", produces = "application/json", response = User.class, value = "Find existing user")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "OK"), @ApiResponse(code = 204, message = "No Content"),
			@ApiResponse(code = 400, message = "Bad Request"), @ApiResponse(code = 401, message = "Unauthorized"),
			@ApiResponse(code = 403, message = "Forbidden"), @ApiResponse(code = 404, message = "Not Found") })
	public ResponseEntity<List<UserDto>> findAll() {
		return ResponseEntity.ok(userService.findAll());
	}
}
