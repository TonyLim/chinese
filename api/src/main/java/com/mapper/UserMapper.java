package com.mapper;

import com.dto.UserDto;
import com.entity.User;

public interface UserMapper {

	User toEntity(UserDto dto);

	UserDto toDto(User entity);
	
}
