package com.mapper;

import com.dto.TranslationDto;
import com.entity.Translation;

public interface TranslationMapper {

	Translation toEntity(TranslationDto dto);

	TranslationDto toDto(Translation entity);
	
}
