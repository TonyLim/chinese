package com.mapper;

import com.dto.LessonDto;
import com.entity.Lesson;

public interface LessonMapper {

	Lesson toEntity(LessonDto dto);

	LessonDto toDto(Lesson entity);
	
}
