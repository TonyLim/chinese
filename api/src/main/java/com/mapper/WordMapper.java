package com.mapper;

import com.dto.WordDto;
import com.entity.Word;

public interface WordMapper {

	Word toEntity(WordDto dto);

	WordDto toDto(Word entity);
	
}
