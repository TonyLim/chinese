package com.mapper.impl;

import org.springframework.stereotype.Component;

import com.dto.UserDto;
import com.entity.User;
import com.mapper.UserMapper;

@Component
public class UserMapperImpl implements UserMapper {

	@Override
	public User toEntity(UserDto dto) {
		User entity = new User();
		entity.setId(dto.getId());
		entity.setFirstName(dto.getFirstName());
		entity.setLastName(dto.getLastName());
		return entity;
	}

	@Override
	public UserDto toDto(User entity) {
		UserDto dto = new UserDto();
		dto.setId(entity.getId());
		dto.setFirstName(entity.getFirstName());
		dto.setLastName(entity.getLastName());
		return dto;
	}

}
