package com.mapper.impl;

import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.dto.WordDto;
import com.entity.Word;
import com.mapper.TranslationMapper;
import com.mapper.WordMapper;

@Component
public class WordMapperImpl implements WordMapper {

	@Autowired
	private TranslationMapper translationMapper;
	
	@Override
	public Word toEntity(WordDto dto) {
		Word entity = new Word();
		entity.setId(dto.getId());
		entity.setPinyin(dto.getPinyin());
		entity.setLogogram(dto.getLogogram());
		//entity.setLesson(lessonMapper.toEntity(dto.getLessonId()));
		entity.setTranslations(dto.getTranslations().stream()
				.map(translation -> translationMapper.toEntity(translation)).collect(Collectors.toList()));
		return entity;
	}
	
	@Override
	public WordDto toDto(Word entity) {
		WordDto dto = new WordDto();
		dto.setId(entity.getId());
		dto.setPinyin(entity.getPinyin());
		dto.setLogogram(entity.getLogogram());
		dto.setLessonId(entity.getLesson().getId());
		dto.setTranslations(entity.getTranslations().stream()
				.map(translation -> translationMapper.toDto(translation)).collect(Collectors.toList()));
		return dto;
	}
}
