package com.mapper.impl;

import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.dto.LessonDto;
import com.entity.Lesson;
import com.mapper.WordMapper;
import com.mapper.LessonMapper;

@Component
public class LessonMapperImpl implements LessonMapper {

	@Autowired
	private WordMapper wordMapper;
	
	@Override
	public Lesson toEntity(LessonDto dto) {
		Lesson entity = new Lesson();
		entity.setId(dto.getId());
		entity.setNumber(dto.getLessonNumber());
		entity.setWords(dto.getWords().stream()
				.map(word -> wordMapper.toEntity(word)).collect(Collectors.toList()));
		return entity;
	}

	@Override
	public LessonDto toDto(Lesson entity) {
		LessonDto dto = new LessonDto();
		dto.setId(entity.getId());
		dto.setLessonNumber(entity.getNumber());
		dto.setWords(entity.getWords().stream()
				.map(word -> wordMapper.toDto(word)).collect(Collectors.toList()));
		return dto;
	}
}
