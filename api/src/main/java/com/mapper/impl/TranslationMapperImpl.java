package com.mapper.impl;

import org.springframework.stereotype.Component;

import com.dto.TranslationDto;
import com.entity.Translation;
import com.mapper.TranslationMapper;

@Component
public class TranslationMapperImpl implements TranslationMapper {
	
	@Override
	public Translation toEntity(TranslationDto dto) {
		Translation entity = new Translation();
		entity.setId(dto.getId());
		//entity.setWord(wordMapper.toEntity(dto.getWordId()));
		entity.setMeaning(dto.getMeaning());
		return entity;
	}

	@Override
	public TranslationDto toDto(Translation entity) {
		TranslationDto dto = new TranslationDto();
		dto.setId(entity.getId());
		dto.setWordId(entity.getWord().getId());
		dto.setMeaning(entity.getMeaning());
		return dto;
	}
}
