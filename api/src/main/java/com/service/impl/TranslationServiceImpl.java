package com.service.impl;

import java.util.List;
import java.util.stream.Collectors;

import javax.websocket.server.ServerEndpoint;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.dto.TranslationDto;
import com.mapper.TranslationMapper;
import com.repository.TranslationRepository;
import com.service.TranslationService;

@Service
@Transactional
public class TranslationServiceImpl implements TranslationService {

	@Autowired
	private TranslationRepository translationRepository;
	@Autowired
	private TranslationMapper translationMapper;
	
	public TranslationServiceImpl(TranslationRepository translationRepository, TranslationMapper translationMapper) {
		this.translationRepository = translationRepository;
		this.translationMapper = translationMapper;
	}

	public List<TranslationDto> findAll() {
		return translationRepository.findAll().stream().map(translationMapper::toDto).collect(Collectors.toList());
	}
	/*
	public List<TranslationDto> findByWordId(Long wordId) {
		return translationRepository.findByWordId(wordId).stream()
				.map(translationMapper::toDto).collect(Collectors.toList());
	}
	*/
	
	public TranslationDto save(TranslationDto dto) {
		return translationMapper.toDto(translationRepository.saveAndFlush(translationMapper.toEntity(dto)));
	}

}
