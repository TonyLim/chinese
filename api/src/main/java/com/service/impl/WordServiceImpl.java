package com.service.impl;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.dto.WordDto;
import com.mapper.WordMapper;
import com.repository.WordRepository;
import com.service.WordService;

@Service
@Transactional
public class WordServiceImpl implements WordService {

	@Autowired
	private WordRepository wordRepository;
	@Autowired
	private WordMapper wordMapper;

	public WordServiceImpl(WordMapper wordMapper, WordRepository wordRepository) {
		this.wordMapper = wordMapper;
		this.wordRepository = wordRepository;
	}
	
	public List<WordDto> findAll() {
		return wordRepository.findAll().stream()
				.map(wordMapper::toDto).collect(Collectors.toList());
	}
	
	public WordDto findById(Long id) {
		return wordMapper.toDto(wordRepository.findById(id).orElse(null));
	}
	
	public List<WordDto> findByLesson(Long lesson) {
		return wordRepository.findByLesson(lesson).orElse(null).stream()
				.map(word -> wordMapper.toDto(word)).collect(Collectors.toList());
	}
	
	public WordDto save(WordDto dto) {
		return wordMapper.toDto(wordRepository.saveAndFlush(wordMapper.toEntity(dto)));
	}

}
