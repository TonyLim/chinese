package com.service.impl;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.dto.UserDto;
import com.mapper.UserMapper;
import com.repository.UserRepository;
import com.service.UserService;

@Service
@Transactional
public class UserServiceImpl implements UserService {

	@Autowired
	private UserRepository userRepository;
	@Autowired
	private UserMapper userMapper;

	public UserServiceImpl(UserMapper userMapper, UserRepository userRepository) {
		this.userMapper = userMapper;
		this.userRepository = userRepository;
	}
	
	public List<UserDto> findAll() {
		return userRepository.findAll().stream().map(userMapper::toDto).collect(Collectors.toList());
	}
	
	public UserDto save(UserDto dto) {
		return userMapper.toDto(userRepository.saveAndFlush(userMapper.toEntity(dto)));
	}

}
