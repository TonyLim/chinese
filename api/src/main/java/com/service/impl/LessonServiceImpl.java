package com.service.impl;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.dto.LessonDto;
import com.mapper.LessonMapper;
import com.repository.LessonRepository;
import com.service.LessonService;

@Service
@Transactional
public class LessonServiceImpl implements LessonService {

	@Autowired
	private LessonRepository lessonRepository;
	@Autowired
	private LessonMapper lessonMapper;

	public LessonServiceImpl(LessonMapper lessonMapper, LessonRepository lessonRepository) {
		this.lessonMapper = lessonMapper;
		this.lessonRepository = lessonRepository;
	}
	
	public List<LessonDto> findAll() {
		return lessonRepository.findAll().stream().map(lessonMapper::toDto).collect(Collectors.toList());
	}
	
	public LessonDto findById(Long id) {
		return lessonMapper.toDto(lessonRepository.findById(id).orElse(null));
	}
	
	public LessonDto save(LessonDto dto) {
		return lessonMapper.toDto(lessonRepository.saveAndFlush(lessonMapper.toEntity(dto)));
	}

}
