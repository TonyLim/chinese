package com.service;

import java.util.List;

import org.springframework.stereotype.Service;
import com.dto.WordDto;

@Service
public interface WordService {
	
	List<WordDto> findAll();
	
	WordDto findById(Long id);
	
	List<WordDto> findByLesson(Long lesson);
	
	WordDto save(WordDto dto);
	
}
