package com.service;

import java.util.List;

import org.springframework.stereotype.Service;
import com.dto.TranslationDto;

@Service
public interface TranslationService {
	
	List<TranslationDto> findAll();
	
	//List<TranslationDto> findByWordId(Long wordId);
	
	TranslationDto save(TranslationDto dto);
	
}
