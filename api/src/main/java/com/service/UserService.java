package com.service;

import java.util.List;

import org.springframework.stereotype.Service;
import com.dto.UserDto;

@Service
public interface UserService {
	
	List<UserDto> findAll();
	
	UserDto save(UserDto dto);
	
}
