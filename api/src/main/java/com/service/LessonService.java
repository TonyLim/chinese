package com.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.dto.LessonDto;

@Service
public interface LessonService {
	
	List<LessonDto> findAll();
	
	LessonDto save(LessonDto dto);
	
}
