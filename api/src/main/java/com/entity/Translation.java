package com.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
@Table(name = "translation")
public class Translation {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "translation_seq")
	@SequenceGenerator(name = "translation_seq", sequenceName = "translation_seq", allocationSize = 1)
	@Column(name = "translation_id", updatable = false)
	private Long id;
    @ManyToOne
	@JoinColumn(name = "word_id", insertable = false, updatable = false)
	private Word word;
	@Column(name = "meaning")
	private String meaning;
}
