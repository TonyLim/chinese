package com.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
@Table(name = "word")
public class Word {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "word_seq")
	@SequenceGenerator(name = "word_seq", sequenceName = "word_seq", allocationSize = 1)
	@Column(name = "word_id", updatable = false)
	private Long id;
	@Column(name = "pinyin")
	private String pinyin;
	@Column(name = "logogram")
	private String logogram;
    @ManyToOne
    @JoinColumn(name = "lesson_id", insertable = false, updatable = false)
	private Lesson lesson;
	@OneToMany(mappedBy = "word", cascade = CascadeType.ALL, orphanRemoval = true)
	private List<Translation> translations = new ArrayList<Translation>();
}
