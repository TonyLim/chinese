import {CommonModule, registerLocaleData} from '@angular/common';
import {NgModule} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import localeFr from '@angular/common/locales/fr';

import {MessageService} from 'primeng/api';
import {MegaMenuModule} from 'primeng/megamenu';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    MegaMenuModule
  ],
  exports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    MegaMenuModule,
  ],
  providers: [
    MessageService,
  ]
})
export class SharedModule {
  constructor() {
    registerLocaleData(localeFr);
  }
}
