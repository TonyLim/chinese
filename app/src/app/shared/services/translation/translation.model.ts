export class Translation {
  public id?: number;
  public wordId?: number;
  public meaning?: string;

  constructor(id?: number, wordId?: number, meaning?: string
  ) {
    this.id = id;
    this.wordId = wordId ? wordId : null;
    this.meaning = meaning ? meaning : null;
  }
}
