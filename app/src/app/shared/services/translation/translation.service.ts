import {Injectable} from '@angular/core';
import {HttpClient, HttpResponse} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';

import {AbstractRESTService} from '../abstractRest.service';
import {environment} from '../../../../environments/environment.prod';
import {Translation} from './translation.model';

@Injectable()
export class TranslationService extends AbstractRESTService {

  private wordUrl = environment.SERVER_API_URL + 'translations';

  constructor(private http: HttpClient) {
    super('translationService', environment.SERVER_API_URL + 'translations');
  }

  getAll(): Observable<Translation[]> {
    return this.http.get<Translation[]>(this.wordUrl);
  }



























}
