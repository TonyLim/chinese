import {Word} from '../word';

export class Lesson {
  public id?: number;
  public lessonNumber?: number;
  public words?: Word[];

  constructor(id?: number, lessonNumber?: number, words?: Word[]
  ) {
    this.id = id;
    this.lessonNumber = lessonNumber ? lessonNumber : null;
    this.words = words ? words : null;
  }
}
