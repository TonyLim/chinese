import {Injectable} from '@angular/core';
import {HttpClient, HttpResponse} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';

import {AbstractRESTService} from '../abstractRest.service';
import {environment} from '../../../../environments/environment.prod';
// import {ToastService} from '@core/toast/toast.service'; TODO why ??
import {Lesson} from './lesson.model';

@Injectable()
export class LessonService extends AbstractRESTService {

  private lessonsUrl = environment.SERVER_API_URL + 'lessons';

  constructor(private http: HttpClient) {
    super('lessonService', environment.SERVER_API_URL + 'lessons');
  }

  getAll(): Observable<Lesson[]> {
    return this.http.get<Lesson[]>(this.lessonsUrl);
  }

}






