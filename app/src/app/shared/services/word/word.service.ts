import {Injectable} from '@angular/core';
import {HttpClient, HttpResponse} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';

import {AbstractRESTService} from '../abstractRest.service';
import {environment} from '../../../../environments/environment.prod';
// import {ToastService} from '@core/toast/toast.service'; TODO why ??
import {Word} from './word.model';

@Injectable()
export class WordService extends AbstractRESTService {

  private wordUrl = environment.SERVER_API_URL + 'words';

  constructor(private http: HttpClient) {
    super('wordService', environment.SERVER_API_URL + 'words');
  }

  getAll(): Observable<Word[]> {
    return this.http.get<Word[]>(this.wordUrl);
  }

  getByLesson(lesson: any): Observable<Word[]> {
    return this.http.get<Word[]>(this.wordUrl + '?lesson=' + lesson);
  }



























}
