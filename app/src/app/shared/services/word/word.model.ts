import { Translation } from '../translation';

export class Word {
  public id?: number;
  public pinyin?: string;
  public logogram?: string;
  public translations?: Translation[];
  public lesson?: number;

  constructor(id?: number, pinyin?: string, logogram?: string, translations?: Translation[], lesson?: number
  ) {
    this.id = id;
    this.pinyin = pinyin ? pinyin : null;
    this.logogram = logogram ? logogram : null;
    this.translations = translations ? translations : null;
    this.lesson = lesson ? lesson : null;
  }
}
