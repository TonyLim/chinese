import {NgModule} from '@angular/core';
import {CommonModule, DatePipe} from '@angular/common';

import {WordService} from './word/word.service';
import {LessonService} from './lesson/lesson.service';

@NgModule({
  declarations: [
  ],
  imports: [
    CommonModule
  ],
  providers: [
    WordService,
    LessonService,
    DatePipe
  ]
})
export class ServicesModule { }
