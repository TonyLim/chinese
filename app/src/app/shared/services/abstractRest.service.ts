export class AbstractRESTService {
  constructor(
    protected serviceName: string,
    protected resourceUrl: string
  ) {
  }

  public getAll() {
    throw new Error(`Service ${this.serviceName} does not implement getAll() method`);
  }

  public getById(id: any) {
    throw new Error(`Service ${this.serviceName} does not implement getById() method`);
  }

  public create(instance: any) {
    throw new Error(`Service ${this.serviceName} does not implement create() method`);
  }

  public update(instance: any) {
    throw new Error(`Service ${this.serviceName} does not implement update() method`);
  }

  public delete(instance: any) {
    throw new Error(`Service ${this.serviceName} does not implement delete() method`);
  }

}
