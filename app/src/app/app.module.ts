import { BrowserModule } from '@angular/platform-browser';
import { NgModule, Injector, APP_INITIALIZER } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { ToastModule } from 'primeng/toast';

import { AppComponent } from './app.component';
import { ServicesModule} from './shared/services/service.module';
import { RouterModule} from '@angular/router';
import { WordListComponent } from './pages/word/word-list/word-list.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {
  MatButtonModule,
  MatFormFieldModule,
  MatInputModule,
  MatRippleModule,
  MatTableModule,
  MatExpansionModule,
  MatIconModule, MatToolbarModule
} from '@angular/material';
import { WordAccordionComponent } from './pages/word/word-accordion/word-accordion.component';

@NgModule({
  declarations: [
    AppComponent,
    WordListComponent,
    WordAccordionComponent
  ],
  imports: [
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    MatRippleModule,
    BrowserModule,
    RouterModule,
    HttpClientModule,
    ServicesModule,
    FormsModule,
    BrowserAnimationsModule,
    MatExpansionModule,
    MatTableModule,
    MatIconModule,
    MatToolbarModule
  ],
  providers: [
    ToastModule
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
