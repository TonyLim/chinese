import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WordAccordionComponent } from './word-accordion.component';

describe('WordAccordionComponent', () => {
  let component: WordAccordionComponent;
  let fixture: ComponentFixture<WordAccordionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WordAccordionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WordAccordionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
