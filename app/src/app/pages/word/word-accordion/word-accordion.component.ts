import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'ch-word-accordion',
  templateUrl: './word-accordion.component.html',
  styleUrls: ['./word-accordion.component.css']
})
export class WordAccordionComponent {
  @Input() dataSource = [];
  @Input() lesson = 0;
}
