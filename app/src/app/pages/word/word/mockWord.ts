import {Lesson} from '../../../shared/services/lesson';

export const MOCK_WORD: Lesson[] = [
  {
    id: 1,
    lessonNumber: 1,
    words: [
      {
        id: 1, pinyin: 'wǒ', logogram: '我', lesson: 1,
        translations: [
          {id: 1, wordId: 1, meaning: 'Moi'},
          {id: 2, wordId: 1, meaning: 'Je'}
        ]
      },
      {
        id: 2, pinyin: 'nǐ', logogram: '伱', lesson: 1,
        translations: [
          {id: 3, wordId: 2, meaning: 'Toi'},
          {id: 4, wordId: 2, meaning: 'Tu'}
        ]
      }
    ]
  },
  {
    id: 2,
    lessonNumber:2 ,
    words: [
      {
        id: 3, pinyin: 'tā', logogram: '他', lesson: 2,
        translations: [
          {id: 5, wordId: 3, meaning: 'Il'}
        ]
      },
      {
        id: 4, pinyin: 'hǎo', logogram: '好', lesson: 2,
        translations: [
          {id: 6, wordId: 4, meaning: 'Bien'}
        ]
      }
    ]
  }
];











