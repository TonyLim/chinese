import {
  Component, DoCheck,
  Input, IterableDiffers,
  OnInit, ViewChild,
} from '@angular/core';
import {MatTable, MatTableDataSource} from '@angular/material';
import {Word} from '../../../shared/services/word';

@Component({
  selector: 'ch-word-list',
  templateUrl: './word-list.component.html',
  styleUrls: ['./word-list.component.css']
})
export class WordListComponent {
  displayedColumns: string[] = ['logogram', 'pinyin', 'traduction'];
  @Input() dataSource: any = [];
  private blacknedCells: any = [];
  private isBlackened: boolean = false;
  private iterableDiffer: any;
  // @ts-ignore
  @ViewChild(MatTable) table: MatTable<any>;

  shuffleData() {
    // tslint:disable-next-line:one-variable-per-declaration
    let currentIndex, temporaryValue, randomIndex;
    currentIndex  = this.dataSource.length;

    // While there remain elements to shuffle...
    while (0 !== currentIndex) {

      // Pick a remaining element...
      randomIndex = Math.floor(Math.random() * currentIndex);
      currentIndex -= 1;

      // And swap it with the current element.
      temporaryValue = this.dataSource[currentIndex];
      this.dataSource[currentIndex] = this.dataSource[randomIndex];
      this.dataSource[randomIndex] = temporaryValue;
    }
    this.table.renderRows();
  }
  mask() {
    this.isBlackened = !this.isBlackened;
    this.blacknedCells = new Array(this.dataSource.length);
    for (let i = 0; i < this.blacknedCells.length; i++) {
      this.blacknedCells[this.dataSource[i].logogram] = Math.floor(Math.random() * 3) + 1;
    }
  }
  unmask() {
    this.isBlackened = !this.isBlackened;
  }
}
