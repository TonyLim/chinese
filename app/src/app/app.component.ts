import {Component, Input, OnInit} from '@angular/core';
import {LessonService} from './shared/services/lesson';
import {MOCK_WORD} from './pages/word/word/mockWord';

@Component({
  selector: 'ch-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'Chinese vocabulary';
  lessonList;
  constructor(private lessonService: LessonService) {
  }

  ngOnInit() {
    this.refresh();
    this.lessonList = MOCK_WORD;
  }

  refresh() {
    this.lessonService.getAll().subscribe((list) => {
      if (list) {
        this.lessonList = list;
      }
    });
  }

}
