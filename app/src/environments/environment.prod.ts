export const environment = {
  production: true,
  VERSION: '1.0.0-SNAPSHOT',
  ENV : 'local',
  SERVER_API_URL: 'https://localhost:8443/',
  DEBUG_INFO_ENABLED: true,
  BUILD_TIMESTAMP: Date.now(),
  REDIRECT_URL: 'https://localhost:4200'
};
